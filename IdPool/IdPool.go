package IdPool

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentity"
)

type IdPool struct {
	client *cognitoidentity.CognitoIdentity
	poolId string
}

type Credential struct {
	accessKey    string
	secretKey    string
	sessionToken string
}

func New(session *session.Session, poolId string) *IdPool {
	return &IdPool{
		client: cognitoidentity.New(session),
		poolId: poolId,
	}
}

func (i IdPool) GetIdWithoutAuth() (string, error) {

	input := &cognitoidentity.GetIdInput{
		IdentityPoolId: aws.String(i.poolId),
	}

	return i.doGetId(input)

}

func (i IdPool) GetIdWithAuth(token string, userPoolId string) (string, error) {

	param := make(map[string]*string)
	provider := fmt.Sprintf("cognito-idp.%v.amazonaws.com/%v", aws.StringValue(i.client.Config.Region), userPoolId)
	param[provider] = aws.String(token)

	input := &cognitoidentity.GetIdInput{
		IdentityPoolId: aws.String(i.poolId),
		Logins:         param,
	}

	return i.doGetId(input)

}

func (i IdPool) doGetId(input *cognitoidentity.GetIdInput) (string, error) {

	out, err := i.client.GetId(input)
	if err != nil {
		return "", err
	}

	return aws.StringValue(out.IdentityId), nil

}

func (i IdPool) GetCredentialWithoutAuth(identityId string) (*Credential, error) {

	input := &cognitoidentity.GetCredentialsForIdentityInput{
		IdentityId: aws.String(identityId),
	}

	return i.doGetCredential(input)

}

func (i IdPool) GetCredentialWithAuth(identityId string, token string, userPoolId string) (*Credential, error) {

	param := make(map[string]*string)
	provider := fmt.Sprintf("cognito-idp.%v.amazonaws.com/%v", aws.StringValue(i.client.Config.Region), userPoolId)
	param[provider] = aws.String(token)

	input := &cognitoidentity.GetCredentialsForIdentityInput{
		IdentityId: aws.String(identityId),
		Logins:     param,
	}

	return i.doGetCredential(input)

}

func (i IdPool) doGetCredential(input *cognitoidentity.GetCredentialsForIdentityInput) (*Credential, error) {

	out, err := i.client.GetCredentialsForIdentity(input)
	if err != nil {
		return nil, err
	}

	return &Credential{
		accessKey:    aws.StringValue(out.Credentials.AccessKeyId),
		secretKey:    aws.StringValue(out.Credentials.SecretKey),
		sessionToken: aws.StringValue(out.Credentials.SessionToken),
	}, nil

}
