module gitlab.com/kuritayu/cognito

go 1.15

require (
	github.com/aws/aws-sdk-go v1.40.49
	github.com/stretchr/testify v1.7.0
)
